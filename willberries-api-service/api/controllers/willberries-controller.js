'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
const jsf = require('json-schema-faker');
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  goods: getGoods,
  cart: getCart,
  popular: getPopular
};



var schema = {
  "type": "array",
  "minItems": 3,
  "maxItems": 5,
  "items": {
    "type": "object",
    "required": [
      "id", "good", "price", "quantity", "total", "totalprice"
    ],
    "properties": {
      "id": {
        "type": "string",
        "faker": "random.number"
      },
      "good": {
        "type": "string",
        "faker": "commerce.productName"
      },
      "price": {
        "type": "string",
        "faker": "commerce.price"
      },
      "quantity": {
        "type": "string",
        "faker": "random.number"
      },
      "total": {
        "type": "string",
        "faker": "random.number"
      },
      "totalprice": {
        "type": "string",
        "faker": "random.number"
      },
    }
  }
}

var goodsschema = {
  "type": "array",
  "minItems": 8,
  "maxItems": 8,
  "items": {
    "type": "object",
    "required": [
      "id", "name", "img", "brand", "color", "price", "adjective", "material"
    ],
    "properties": {
      "id": {
        "type": "integer",
        "faker": "random.number"
      },
      "name": {
        "type": "string",
        "faker": "commerce.productName"
      },
      "img": {
        "type": "string",
        "faker": "image.fashion"
      },
      "brand": {
        "type": "string",
        "faker": "company.companyName"
      },
      "color": {
        "type": "string",
        "faker": "commerce.color"
      },
      "price": {
        "type": "string",
        "faker": "commerce.price"
      },
      "adjective": {
        "type": "string",
        "faker": "commerce.productAdjective"
      },
      "material": {
        "type": "string",
        "faker": "commerce.productMaterial"
      },

    }
  }
}
/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */



function getPopular(req, res) {
  var name = req.swagger.params.name.value || 'stranger';
  var hello = util.format('Hello, %s!', name);
  res.json([
    {
      "id": faker.datatype.number(),
      "name": faker.commerce.productName(),
      "img": faker.image.fashion(),
      "brand": faker.company.companyName(),
      "color": ''+ faker.commerce.color(),
      "price": ''+ faker.commerce.price(),
      "adjective":faker.commerce.productAdjective(),
      "material": faker.commerce.productMaterial(),
    }
  ]);
}
/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getCart(req, res) {
  var name = req.swagger.params.name.value || '2020-03-01';
  var title = util.format('Hey %s', name);
    
  jsf.resolve(schema).then(sample => res.json(sample));
}

function getGoods(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var name = req.swagger.params.name.value || 'stranger';
  var hello = util.format('Hello, %s!', name);

  // this sends back a JSON response which is a single string
  jsf.resolve(goodsschema).then(sample => res.json(sample));
}
