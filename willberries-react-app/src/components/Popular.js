import arrow from '../img/arrow.svg';
import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';



const api = new Api.DefaultApi()

class Popular extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            popular: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }

    
    async handleReload(popular) { //эта функция отвечает за обновление данных при перезагрузке страницы
        const response = await api.popular({ name: '' });
        this.setState({ popular: response });
    }


    render(){
        return <div>
        <section class="special-offers container pt-5 pb-4">
		<div class="row mb-4">
            <div class="col-xl-6">
            {this.state.popular.map(
								(event) =>
                <div class="card card-1 mb-4">
                    <h3 class="card-title">Fashion Month Ready in Capital Shop</h3>
                    <p class="card-text">{event.brand} & {event.material} & {event.adjective} & {event.color}</p>
                    <button class="button">
                        <span class="button-text">View all</span>
                        <img src={arrow} alt="icon: arrow" class="button-icon"></img>
                    </button>
                 </div>)
    }
            </div>
			
			<div class="col-xl-6">
            {this.state.popular.map(
								(event) =>
				<div class="card card-2 mb-4" style={{backgroundImage: `url(${event.img})`}}>
                    
                        <h3 class="card-title text-light">{event.adjective} {event.color} {event.name}
                        </h3>
					<p class="card-text text-light">{event.brand} & {event.material} & {event.adjective} & {event.color}</p>

					<button class="button">
						<span class="button-text">View all</span>
						<img src={arrow} alt="icon: arrow" class="button-icon"></img>
					</button>
				</div>)
    }	
			</div>
			
			<div class="col-xl-9 col-lg-6 mb-4">
            {this.state.popular.map(
                            (event) =>
				<div class="card card-3">
					<span class="label">Bestseller</span>
					<h3 class="card-title large">Poplin Top <br></br>With Sleeve Bow</h3>
					<p class="card-text large">Poplin top with roll neckline, long sleeves</p>
					<button class="button add-to-cart" data-id="002">
						<span class="button-price">${event.price}</span>
						<span class="button-text">Shop now</span>
					</button>
				</div>)
            }   	
			</div>
			
			<div class="col-xl-3 col-lg-6">
            {this.state.popular.map(
                            (event) =>
				<div class="card card-4">
					<h3 class="card-title text-light mw-160">Printed Shirt with a Bow</h3>
					<p class="card-text text-light">Pink/Sky Blue/Yellow</p>
					<button class="button add-to-cart button-four" data-id="009">
						<span class="button-price">${event.price}</span>
						<span class="button-text">Shop now</span>
					</button>
				</div>)
            }   	
			</div>
			
		</div>
	</section>
        </div>
    }
}


export default Popular;