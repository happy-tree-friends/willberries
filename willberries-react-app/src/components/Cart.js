import arrow from '../img/arrow.svg';
import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';
import { Link } from 'react-router-dom'



const api = new Api.DefaultApi()

class Cart extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            cart: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(cart) { //эта функция отвечает за обновление данных при перезагрузке страницы
        const response = await api.cart({ name: '' });
        this.setState({ cart: response });
    }

    render() {
        return <div>
        <div class="overlay" id="modal-cart">
                <div class="modal">
                    <header class="modal-header">
                        <h2 class="modal-title">Cart</h2>
                        <button class="modal-close"> {<Link to={'/'}>x</Link>}</button>
                    </header>
                    <table class="cart-table">
                        <colgroup>
                            <col class="col-goods"></col>
                            <col class="col-price"></col>
                            <col class="col-minus"></col>
                            <col class="col-qty"></col>
                            <col class="col-plus"></col>
                            <col class="col-total-price"></col>
                            <col class="col-delete"></col>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Good(s)</th>
                            <th>Price</th>
                            <th colspan="3">Qty.</th>
                            <th colspan="2">Total</th>
                        </tr>
                        </thead>
                        <tbody class="cart-table__goods">
                            {this.state.cart.map(
                                (event) => 
                                <tr class="cart-item" data-id="003">
                                    <td>{event.good}</td>
                                    <td>${event.price}</td>
                                    <td><button class="cart-btn-minus">-</button></td>
                                    <td>{event.quantity}</td>
                                    <td><button class="cart-btn-plus">+</button></td>
                                    <td>${event.totalprice}</td>
                                    <td><button class="cart-btn-delete">x</button></td>
                                </tr>
                                )

                            }
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">Total:</th>
                            <th class="card-table__total" colspan="2">$1500</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    }
}


export default Cart;