import arrow from '../img/arrow.svg';
import image122 from '../img/image-122.jpg';
import image121 from '../img/image-121.jpg';
import image120 from '../img/image-120.jpg';
import image119 from '../img/image-119.jpg';
import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';

const api = new Api.DefaultApi()

class Goods extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            goods: []
            
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(goods) { //эта функция отвечает за обновление данных при перезагрузке страницы
        const response = await api.goods({ name: '' });
        this.setState({ goods: response });
    }

    render() {
        return <div>
            <section class="long-goods">
            <div class="container">
                <div class="row align-items-center mb-4">
                    <div class="col-12">
                        <h2 class="section-title">Category</h2>
                    </div>
                </div>
                <div class="row long-goods-list">
                {this.state.goods.map(
                        (event) => 
                        <div class="col-lg-3 col-sm-6">
 <div class="goods-card">
     <span class="label">New</span>
     <img src={event.img} alt="image: image122" class="goods-image"></img>
     <h3 class="goods-title">{event.name} from {event.brand}</h3>
     <p class="goods-description">{event.adjective}/{event.color}/{event.material}</p>
     <button class="button goods-card-btn add-to-cart" data-id="001">
         <span class="button-price">${event.price}</span>
     </button>
 </div>
</div>
            )

                }
 
 <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image122} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Striped Long Sleeve Shirt from Parker</h3>
                <p class="goods-description">Gorgeous/Lavender/Fresh</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price">$119.00</span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image121} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Garment Dyed Heavyweight Ringspun T-Shirt from Collins</h3>
                <p class="goods-description">Gorgeous/White/Soft</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price">$315.00</span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image120} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Brown Cropped Trousers from Kuhn and Sons</h3>
                <p class="goods-description">Fantastic/Brown/Cotton</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price">$206.00</span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image119} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">The Modern-Day Hoodie from Swift</h3>
                <p class="goods-description">Amazing/Purple/Soft</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price">$286.00</span>
                </button>
            </div>
        </div>

                </div>    
               
            </div>
        </section>
            </div>
    }
}

export default Goods;