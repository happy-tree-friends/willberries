import './css/style.css';
import './css/bootstrap-grid.min.css';
import './css/swiper-bundle.min.css';
import arrow from './img/arrow.svg';
import imgfacebook from './img/Facebook.svg';
import imgtwitter from './img/Twitter.svg';
import imginstagram from './img/Instagram.svg';
import imgvisa from './img/visa.png';
import imgmastercard from './img/master-card.png';
import imgpaypal from './img/paypal.png';
import imgbitcoin from './img/bitcoin.png';
import arrowtop from './img/top.svg';
import logo from './img/logo.svg';
import cart from './img/cart.svg';
import image122 from './img/image-122.jpg';
import image121 from './img/image-121.jpg';
import image120 from './img/image-120.jpg';
import image119 from './img/image-119.jpg';
import arrownext from './img/arrow-next.svg';
import arrowprev from './img/arrow-prev.svg';
import './App.css';
import React from 'react';
import Cart from './components/Cart';
import Goods from './components/Goods';
import Popular from './components/Popular';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";
import moment from 'moment'
import imagewho from './img/who.png';
import choice from './img/choice.jpg';
import Delivery from './img/Delivery.png';

function App() {
  return (
    <Router>
    <div>
                  <header class="container header px-4 px-md-0">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-lg-2 col-6">
                                <a href="index.html" class="logo-link">
                                    <img width="128" src={logo} alt="logo: Willberries" class="logo-image"></img>
                                </a>
                            </div>
                            <div class="col-lg-6 d-none d-lg-block">
                                <nav>
                                    <ul class="navigation d-flex justify-content-around">
                                        <li class="navigation-item">
                                            <Link to="/popular" class="navigation-link">Popular</Link>
                                        </li>
                                        <li class="navigation-item">
                                          <Link to="/goods" class="navigation-link">Goods</Link>
                                        </li>
                                        <li class="navigation-item">
                                          <Link to="/about" class="navigation-link">About us</Link>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-lg-2 col-6 d-flex justify-content-end">
                                <button class="button button-cart">
                                <Link to="/cart" class="button-text">
                                    <img class="button-icon" src={cart} alt="icon: cart"></img>
                                    <span class="button-text">Cart</span>
                                    <span class="button-text cart-count"></span>
                                  </Link>
                                </button>
                            </div>
                        </div>
	                </header>  
      <div className="App">
         
        <section> 
          <Switch>
             <Route path="/popular">
              <Popular />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/goods">
              <Goods />
            </Route>
            <Route path="/about">
              <h1 class="header__title"> Who we are.</h1>
              <p></p>
             <p class="paragraphs">We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. To be brave and grab life as the extraordinary adventure it is. So we make sure everyone has an equal chance to discover all the amazing things they’re capable of – no matter who they are, where they’re from or what looks they like to boss. We exist to give you the confidence to be whoever you want to be. </p>
             <img src={imagewho} alt="image: Str" class="imagewho"></img>
             <h2>Choice for all</h2>
                        <p class="paragraphs">Our audience is wonderfully unique. And we do everything we can to help you find your fit, offering our brands in more than 30 sizes – and we're committed to providing all sizes at the same price – so you can be confident we’ve got the perfect thing for you. We’re also proud to partner with GLAAD, one of the biggest voices in LGBTQ activism, on a gender-neutral collection to unite in accelerating acceptance. </p>
                        <img src={choice} alt="image: Str" class="imagewho"></img>
                        <img src={Delivery} alt="image: Str" class="imagewho"></img>
             <p class="paragraphs">We partnered with Global-e, a third party service acting as your seller-on-record, so you can buy and ship your Yours Clothing products to over 100 destinations worldwide! When you buy your Yours Clothing products from Global-e, you will see that your payment method is charged by **Global-e// Yours Clothing **, and the purchase is subject to Global-e’s Terms and Conditions and Privacy Policy (which will be clearly presented in checkout before you place the order).</p>
            <h2>Untracked Standard Delivery</h2> 
            <p>Delivered in 8-10 working days <b>$10</b></p>
            <h2>Tracked Express Delivery</h2>
            <p>Delivered in 1-2 working days <b>$25</b></p>
            </Route>
            <Route path="/">
            <section class="slider swiper-container">
                        <div class="swiper-wrapper">
                            <section class="slide slide-1 swiper-slide">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 col-10 offset-lg-1">
                                            <span class="label">Bestseller</span>
                                            <h2 class="slide-title">Women's Alpargata Loafer</h2>
                                            <p class="slide-description">At Alpa believe in a better tomorrow, one where humanity thrives.</p>
                                            <button class="button add-to-cart" data-id="003">
                                                <span class="button-price"><Link to="/goods" class="button-price">$219</Link></span>
                                                <span class="button-text">Shop now</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                      
                            <section class="slide slide-1 swiper-slide">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 col-10 offset-lg-1">
                                            <span class="label">Bestseller</span>
                                            <h2 class="slide-title">Sweater Choker Neck</h2>
                                            <p class="slide-description">Women’s pearl basic knit sweater with a round neck. Available in several colours. Free shipping to stores.</p>
                                            <button class="button add-to-cart" data-id="005">
                                                <span class="button-price">{<Link to="/goods" class="button-price">$319</Link>}</span>
                                                <span class="button-text">Shop now</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="slider-nav">
                            <div class="container">
                                <div class="row justify-content-between">
                                    <div class="col-1">
                                        <button class="slider-button slider-button-prev">
                                            <img src={arrowprev} alt="icon: arrow-prev"></img>
                                        </button>
                                    </div>
                                    <div class="col-1">
                                        <button class="slider-button slider-button-next">
                                            <img src={arrownext} alt="icon: arrow-prev"></img>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



    <section class="special-offers container pt-5 pb-4">
    <div class="row mb-4">

          <div class="col-xl-6">
            <div class="card card-1 mb-4">
              <h3 class="card-title">Fashion Month Ready in Capital Shop</h3>
              <p class="card-text">Bags & Acsessories & Lingerie & Sportswear & Beauty & Swimwear</p>
              <button class="button">
                <span class="button-text"><Link to="/popular" class="button-text">View all</Link></span>
                <img src={arrow} alt="icon: arrow" class="button-icon"></img>
              </button>
            </div>
          </div>

          <div class="col-xl-6">
            <div class="card card-2 mb-4">
              <h3 class="card-title text-light">Catch the Sun: Spring Break Styles From $5.99</h3>
              <p class="card-text text-light">Sweaters & Hoodies & Puffer Jackets & Coats and Jackets & Knit</p>
              <button class="button">
                <span class="button-text"><Link to="/popular" class="button-text">View all</Link></span>
                <img src={arrow} alt="icon: arrow" class="button-icon"></img>
              </button>
            </div>
          </div>

    </div>
    </section>





<section class="long-goods">
	<div class="container"> 
    <div class="row long-goods-list">


        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image122} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Striped Long Sleeve Shirt from Parker</h3>
                <p class="goods-description">Gorgeous/Lavender/Fresh</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price"><Link to="/goods" class="button-price">$119.00</Link></span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image121} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Garment Dyed Heavyweight Ringspun T-Shirt from Collins</h3>
                <p class="goods-description">Gorgeous/White/Soft</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price"><Link to="/goods" class="button-price">$315.00</Link></span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image120} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">Brown Cropped Trousers from Kuhn and Sons</h3>
                <p class="goods-description">Fantastic/Brown/Cotton</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price"><Link to="/goods" class="button-price">$206.00</Link></span>
                </button>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="goods-card">
                <span class="label">New</span>
                <img src={image119} alt="image: Str" class="goods-image"></img>
                <h3 class="goods-title">The Modern-Day Hoodie from Swift</h3>
                <p class="goods-description">Amazing/Purple/Soft</p>
                <button class="button goods-card-btn add-to-cart" data-id="001">
                    <span class="button-price"><Link to="/goods" class="button-price">$286.00</Link></span>
                </button>
            </div>
        </div>

    </div>  
  </div>
</section>





            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>  
        </section>
      </div>
      
      <footer class="footer">
                    <div class="container">
                      <div class="row">
                        <div class="col-xl-7 mb-4 mb-xl-0">
                          <nav>
                            <ul class="footer-menu d-sm-flex">
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Shop</a></li>
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">About Us</a></li>
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Careers</a></li>
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">FAQ </a></li>
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Blog</a></li>
                              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Contacts</a></li>
                            </ul>
                          </nav>
                        </div>

                        <div class="col-lg-3 d-flex align-items-center">
                          <span class="footer-text">Follow Us</span>
                          <span class="footer-social d-inline-flex align-items-center">
                            <a href="#" class="social-link">
                              <img src={imgfacebook} alt="Facebook"></img>
                            </a>
                            <a href="#" class="social-link">
                              <img src={imgtwitter} alt="Twitter"></img>
                            </a>
                            <a href="#" class="social-link">
                              <img src={imginstagram} alt="Instagram"></img>
                            </a>
                          </span>
                        </div>

                        <div class="col-xl-2 col-lg-3 d-flex justify-content-lg-end mt-4 mt-lg-0">
                          <span class="footer-text">© 2021 HTF+</span>
                        </div>

                      </div>

                      <hr class="footer-line mt-4 mb-4"></hr>
                      <div class="row justify-content-between">
                        <div class="col-lg-4 col-sm-9">
                          <img class="payment-logo" src={imgvisa} alt="visa logo"></img>
                          <img class="payment-logo" src={imgmastercard} alt="master card logo"></img>
                          <img class="payment-logo" src={imgpaypal} alt="paypal logo "></img>
                          <img class="payment-logo" src={imgbitcoin} alt="bitcoin logo"></img>
                        </div>

                        <div class="col-lg-2 col-sm-1 d-flex justify-content-end">
                          <a href="#body" class="scroll-link top-link d-flex align-items-center">
                            <span class="top-link-text">Top</span>
                            <img src={arrowtop} alt="icon: arrow top"></img>
                          </a>
                        </div>

                      </div>

                    </div>

       </footer>

    </div>
    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}
export default App;
